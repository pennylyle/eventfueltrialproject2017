import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconColor',
  pure: false
})
export class HuePipe implements PipeTransform {

  constructor() {}

  transform(value: string): any {
    var ext = value.toLowerCase();

    if(value.match(/^[^a-z]*$/i)) {
      return 'icon-default';
    }
    return 'icon-' + ext.charAt(0);
  }

}
