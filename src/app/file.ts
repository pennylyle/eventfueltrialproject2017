export class File {
  id: number;
  name: string;
  updated_at: string;
  extension: string;
  source: string;
}
