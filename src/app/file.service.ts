import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { File } from './file';
import { MessageService } from './message.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class FileService {
  /** URL to web api - original JSON example */
  //private filesUrl = 'https://api.myjson.com/bins/q89f3';

  /** URL to web api - example JSON containing extensions */
  private filesUrl = 'https://api.myjson.com/bins/a0ppb';

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** Get list of files from the server */
  getFiles (): Observable<File[]> {
    return this.http.get<File[]>(this.filesUrl)
  }

  /** Log a FileService message with the MessageService */
  private log(message: string) {
    this.messageService.add('FileService: ' + message);
  }
}
