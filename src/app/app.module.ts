import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FilesComponent } from './files/files.component';
import { FileService } from './file.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { HuePipe } from './hue.pipe';
import { SafePipe } from './safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
    MessagesComponent,
    HuePipe,
    SafePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
  	FileService,
  	MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
